package response;

import model.Service;
import model.Type_service;

public class ServiceResponse {

	private long id;
	private double brutePrice;
	private String name;
	private int iva;
	private TypeServiceResponse typeService;

	public ServiceResponse(long id, double brutePrice, String name, int iva, Type_service typeService) {
		super();
		this.id = id;
		this.brutePrice = brutePrice;
		this.name = name;
		this.iva = iva;
		this.typeService = new TypeServiceResponse(typeService);
	}

	public ServiceResponse(Service service) {
		super();
		this.id = service.getId();
		this.brutePrice = service.getBrute_price();
		this.name = service.getName();
		this.iva = service.getIva();
		this.typeService = new TypeServiceResponse(service.getType_service());
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public double getBrutePrice() {
		return brutePrice;
	}

	public void setBrutePrice(double brutePrice) {
		this.brutePrice = brutePrice;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getIva() {
		return iva;
	}

	public void setIva(int iva) {
		this.iva = iva;
	}

	public TypeServiceResponse getTypeService() {
		return typeService;
	}

	public void setTypeService(TypeServiceResponse typeService) {
		this.typeService = typeService;
	}

}
