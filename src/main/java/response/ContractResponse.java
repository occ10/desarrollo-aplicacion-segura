package response;

import java.sql.Date;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import model.Contract;
import model.Service;
import model.UserDTO;

public class ContractResponse {

	private long id;
	private Date date;
	private double precioTotal;
	private UserDTO user;
	private List<ServiceResponse> services;

	public ContractResponse(Contract contract) {
		this.id = contract.getId();
		this.date = contract.getDate();
		this.user = new UserDTO(contract.getUser());
		Iterable<Service> services = contract.getServices();
        this.precioTotal = calcularPrecioTotal(contract.getServices());
		this.services = StreamSupport.stream(services.spliterator(), true)
                .map(u -> new ServiceResponse(u))
                .collect(Collectors.toList());
	}

	private double calcularPrecioTotal(Collection<Service> services) {
		double precioTotal = 0;
        for (Service s : services) {
        	precioTotal += s.getBrute_price() * (s.getIva() * 0.01);
        }
        
		return precioTotal;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	
	public double getPrecioTotal() {
		return precioTotal;
	}

	public void setPrecioTotal(double precioTotal) {
		this.precioTotal = precioTotal;
	}

	public UserDTO getUser() {
		return user;
	}

	public void setUser(UserDTO user) {
		this.user = user;
	}

	public List<ServiceResponse> getServices() {
		return services;
	}

	public void setServices(List<ServiceResponse> services) {
		this.services = services;
	}
	

}
