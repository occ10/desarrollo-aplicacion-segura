package response;

import model.Type_service;

public class TypeServiceResponse {

	private long id;
	private String name;

	public TypeServiceResponse(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public TypeServiceResponse(Type_service typeService) {
		super();
		this.id = typeService.getId();
		this.name = typeService.getName();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
