package response;

import model.Users;

public class UserResponse {
	
    private long id;

    private String email;
    private String phoneNumber;

    public UserResponse(Users user) {
        this.id = user.getId();
        this.email = user.getEmail();
        this.phoneNumber = user.getPhone();
    }

    public long getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
    
    
}
