package crypto;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.security.NoSuchAlgorithmException;
import org.bouncycastle.util.encoders.Base64;
import static java.nio.charset.StandardCharsets.UTF_8;

public class AESKeyGenerator {

	
	public static SecretKeySpec generate(byte[] input) {
		byte[] hash = SHA3Generator.fromByte(input);

		return new SecretKeySpec(hash, "AES");
	}

	public static SecretKey random() throws NoSuchAlgorithmException {
		KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
		keyGenerator.init(256);
		return keyGenerator.generateKey();
	}

	public static String encryptAes(String plainText, SecretKey key) throws Exception {
		Cipher encryptCipher = Cipher.getInstance("AES");
		encryptCipher.init(Cipher.ENCRYPT_MODE, key);

		byte[] cipherText = encryptCipher.doFinal(plainText.getBytes(UTF_8));

		return new String (Base64.encode(cipherText));
	}

    public static String decryptAes(String cipherText, SecretKey key) throws Exception {
        byte[] bytes = Base64.decode(cipherText);

        Cipher decriptCipher = Cipher.getInstance("AES");
        decriptCipher.init(Cipher.DECRYPT_MODE, key);

        return new String(decriptCipher.doFinal(bytes), UTF_8);
    }

}
