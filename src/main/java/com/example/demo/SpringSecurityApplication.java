package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import security.Argon2PasswordEncoder;
import security.SecurityConfig;

@SpringBootApplication
@ComponentScan({"com.example.demo","controller"})
@ComponentScan({"com.example.demo","security"})
@ComponentScan({"com.example.demo","exception"})
@EntityScan(basePackages = "model")
@ComponentScan({"com.example.demo","response"})
@ComponentScan({"com.example.demo","service"})
@ComponentScan({"com.example.demo","crypto"})
@EnableJpaRepositories("repository")

public class SpringSecurityApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringSecurityApplication.class, args);
	}
	
	@Bean
	public Argon2PasswordEncoder argon2PasswordEncoder() {
	    return new Argon2PasswordEncoder();
    }
}
