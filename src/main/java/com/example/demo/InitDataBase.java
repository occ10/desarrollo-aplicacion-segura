package com.example.demo;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import model.Service;
import model.Type_service;
import repository.ServiceRepository;
import repository.TypeServiceRepository;

@Component
public class InitDataBase implements
ApplicationListener<ContextRefreshedEvent> {

    boolean alreadySetup = true;
    
	@Autowired
    private TypeServiceRepository typeServiceRepository;

	@Autowired
    private ServiceRepository serviceRepository;
	
	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
        
        if (alreadySetup)
            return;
        
        createTypeServiceIfNotFound("Internet");
        createTypeServiceIfNotFound("Telefonía Fija");
        createTypeServiceIfNotFound("Telefonía Movil"); 
        createTypeServiceIfNotFound("Otros");       

        Type_service typeService = typeServiceRepository.findByName("Internet");
        Service service = new Service();
        service.setName("TARIFA PLUS 12/4");
        service.setBrute_price(39.6700);
        service.setType_service(typeService);
        service.setIva(21);
        serviceRepository.save(service);
        
        service = new Service();
        service.setName("TARIFA ECO 6/2");
        service.setBrute_price(19.8400);
        service.setType_service(typeService);
        service.setIva(21);
        serviceRepository.save(service);

        service = new Service();
        service.setName("TARIFA BASICA 8/3");
        service.setBrute_price(29.7500);
        service.setType_service(typeService);
        service.setIva(21);
        serviceRepository.save(service);

        service = new Service();
        service.setName("Conexión a Internet garantizada 40 UL + ");
        service.setBrute_price(299.0000);
        service.setType_service(typeService);
        service.setIva(21);
        serviceRepository.save(service);

        service = new Service();
        service.setName("CREDITOS");
        service.setBrute_price(0.0000);
        service.setType_service(typeService);
        service.setIva(21);
        serviceRepository.save(service);
        
        service = new Service();
        service.setName("TARIFA SIMéTRICA 10/10");
        service.setBrute_price(99.0000);
        service.setType_service(typeService);
        service.setIva(21);
        serviceRepository.save(service);        

        service = new Service();
        service.setName("Tarifa Profesional 10/10");
        service.setBrute_price(99.1700);
        service.setType_service(typeService);
        service.setIva(21);
        serviceRepository.save(service);
    
        service = new Service();
        service.setName("CONEXIóN WIFI PúBLICA");
        service.setBrute_price(100.0000);
        service.setType_service(typeService);
        service.setIva(21);
        serviceRepository.save(service);
        
        alreadySetup = true;
	}

	@Transactional
    private Type_service createTypeServiceIfNotFound(String name) {
		
		Type_service typeService = new Type_service();
        if (typeServiceRepository.findByName(name) == null) {
        	typeService.setName(name);
        	typeServiceRepository.save(typeService);
        }
        return typeService;
    }
	
	@Transactional
    private Service creatServiceIfNotFound(String name) {
		
		Service service = new Service();
        if (serviceRepository.findByName(name) == null) {
        	service.setName(name);
        	serviceRepository.save(service);
        }
        return service;
    }
}

