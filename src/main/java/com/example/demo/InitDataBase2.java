package com.example.demo;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import model.Service;
import model.Type_service;
import repository.ServiceRepository;
import repository.TypeServiceRepository;

@Component
public class InitDataBase2 implements
ApplicationListener<ContextRefreshedEvent> {

    boolean alreadySetup = true;
    
	@Autowired
    private TypeServiceRepository typeServiceRepository;

	@Autowired
    private ServiceRepository serviceRepository;
	
	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
        
        if (alreadySetup)
            return;
        
        Type_service typeService = typeServiceRepository.findByName("Telefonía Fija");
        Service service = new Service();
        service.setName("FIJO");
        service.setBrute_price(9.0900);
        service.setType_service(typeService);
        service.setIva(21);
        serviceRepository.save(service);
        
        service = new Service();
        service.setName("Fijo Tarifa Internacional (TS)");
        service.setBrute_price(20.0000);
        service.setType_service(typeService);
        service.setIva(21);
        serviceRepository.save(service);

        service = new Service();
        service.setName("Fijo 1000 (TS)");
        service.setBrute_price(12.0000);
        service.setType_service(typeService);
        service.setIva(21);
        serviceRepository.save(service);
        
        alreadySetup = true;
	}

	@Transactional
    private Type_service createTypeServiceIfNotFound(String name) {
		
		Type_service typeService = new Type_service();
        if (typeServiceRepository.findByName(name) == null) {
        	typeService.setName(name);
        	typeServiceRepository.save(typeService);
        }
        return typeService;
    }
}

