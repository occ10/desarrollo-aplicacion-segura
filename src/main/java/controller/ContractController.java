package controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import model.Contract;
import response.ContractResponse;
import service.ContractService;

@RestController
@RequestMapping(path = "/contracts")
public class ContractController {
	Logger logger = LoggerFactory.getLogger(ContractController.class);

	@Autowired
	private ContractService contractService;

	public ContractController(ContractService contractService) {
		this.contractService = contractService;
	}

	@GetMapping("/user/{user_id}")
	@ResponseBody
	public List<ContractResponse> getUserContract(@PathVariable("id") Long user_id) {
		List<ContractResponse> contracts = contractService.getUserContracts(user_id);
		return contracts;
	}

	@GetMapping("/{id}")
	@ResponseBody
	public ContractResponse geContract(@PathVariable("id") Long id) {
		 logger.info("Se ha consultado contrato con identificador:" + id);
		ContractResponse contract = contractService.getContract(id);
		return contract;
	}
}
