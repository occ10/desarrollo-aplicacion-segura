package controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestSecurityController {

	 @RequestMapping(value = "/")
	    public String  hello() {
	        return "hello world";
	    }

	 @RequestMapping(value = "/noprotected")
	    public String  helloAgain() {
	        return "hello from no protected role";
	    }
	 
	 @RequestMapping(value = "/protectedByUserRole")
	    public String  helloUser() {
	        return "hello User Role";
	    }
	 @PreAuthorize("hasRole('ROLE_ADMIN')")
	 @GetMapping(value = "/protectedByAdminRole")
	    public String  helloAdmin() {
	        return "hello Admin Role";
	    }
	 @PreAuthorize("hasAuthority('READ_PRIVILEGE')")
	 @RequestMapping(value = "/readPrivelege")
	    public String  readPrivilege() {
	        return "hello read privilege";
	    }

	 @PreAuthorize("hasAuthority('WRITE_PRIVILEGE')")
	 @RequestMapping(value = "/writePrivelege")
	    public String  writePrivilege() {
	        return "hello write privilege";
	    }
	 
}
