package controller;

import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import exeption.UserAlreadyExistsException;
import model.Contract;
import model.Users;
import repository.UserRepository;
import response.ContractResponse;
import response.UserResponse;
import service.UserService;


@RestController
@RequestMapping(path = "/users")
public class UserController {
	Logger logger = LoggerFactory.getLogger(UserController.class);
	
	@Autowired
    private UserService userService;
	@Autowired
    private UserRepository userRepository;

    public UserController(UserService userService, UserRepository userRepository) {
        this.userService = userService;
        this.userRepository = userRepository;
    }

    @PostMapping
    @ResponseBody
    public UserResponse create(
            @RequestBody Users user
    ) throws NoSuchAlgorithmException, UserAlreadyExistsException {
    	
    	logger.info("UserController, Method 'create'({})'", user.getUsername());
        UserResponse userResponse = new UserResponse(userService.create(user));
        return userResponse;
    }

    @GetMapping
    @ResponseBody
    public List<UserResponse> getAll() {
    	
    	logger.info("UserController, Method 'getAll'({})'", "");
        Iterable<Users> users = userRepository.findAll();
        return StreamSupport.stream(users.spliterator(), false)
                .map(UserResponse::new)
                .collect(Collectors.toList());
    }

    @RequestMapping(value = "/bills", method = RequestMethod.GET)
    @ResponseBody
    public List<ContractResponse> getUserContracts() {
    	
    	logger.info("UserController, Method 'getUserContracts'({})'", "");
         List<ContractResponse> contracts = userService.getUserContract();
         
         return contracts;
 
    }
    
}
