package repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import model.Users;

@Repository 
public interface UserRepository extends CrudRepository<Users, Long> {
    Users findByEmail(String email);
    //Users findById(Long id);
}
