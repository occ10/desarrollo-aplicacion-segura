package repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import model.Contract;

public interface ContractRepository extends CrudRepository<Contract, Long> {
	public List<Contract> findByUserId(Long user_id);
	Optional<Contract> findById(Long id);
}
