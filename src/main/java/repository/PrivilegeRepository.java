package repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import model.Privilege;

@Repository 
public interface PrivilegeRepository extends CrudRepository<Privilege, Long> {
	Privilege findByName(String name);
}

