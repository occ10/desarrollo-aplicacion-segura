package repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import model.Type_service;

@Repository 
public interface TypeServiceRepository extends CrudRepository<Type_service, Long> {
	Type_service findByName(String name);
}