package model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "services")
public class Service implements Serializable{
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "brute_price")
	private Double brute_price;

    @Column(name = "name")
	private String name;

    @Column(name = "iva")
	private int iva;
    
    @ManyToMany(mappedBy = "services")
    Collection<Contract> contracts;
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "type_service")
    private Type_service type_service;

	public Service() {}

	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public Double getBrute_price() {
		return brute_price;
	}


	public void setBrute_price(Double brute_price) {
		this.brute_price = brute_price;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public int getIva() {
		return iva;
	}

	public void setIva(int i) {
		this.iva = i;
	}

	public Collection<Contract> getContracts() {
		return contracts;
	}

	public void setContracts(Collection<Contract> contracts) {
		this.contracts = contracts;
	}

	public Type_service getType_service() {
		return type_service;
	}


	public void setType_service(Type_service type_service) {
		this.type_service = type_service;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((brute_price == null) ? 0 : brute_price.hashCode());
		result = prime * result + ((contracts == null) ? 0 : contracts.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((type_service == null) ? 0 : type_service.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Service other = (Service) obj;
		if (brute_price == null) {
			if (other.brute_price != null)
				return false;
		} else if (!brute_price.equals(other.brute_price))
			return false;
		if (contracts == null) {
			if (other.contracts != null)
				return false;
		} else if (!contracts.equals(other.contracts))
			return false;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (type_service == null) {
			if (other.type_service != null)
				return false;
		} else if (!type_service.equals(other.type_service))
			return false;
		return true;
	}

}
