package model;

import java.io.Serializable;

public class UserDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private String email;
	private String username;
	private boolean enabled;

	public UserDTO() {
		super();
	}

	public UserDTO(String email, String username, boolean enabled) {
		super();
		this.email = email;
		this.username = username;
		this.enabled = enabled;
	}

	public UserDTO(Users user) {
		super();
		this.email = user.getEmail();
		this.username = user.getUsername();
		this.enabled = user.isEnabled();
	}
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
