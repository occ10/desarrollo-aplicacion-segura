package service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import model.Contract;
import repository.ContractRepository;
import response.ContractResponse;

@Service
public class ContractService {

	@Autowired
	private ContractRepository contractRepository;

	public ContractService(ContractRepository contractRepository) {
		this.contractRepository = contractRepository;
	}

	public List<ContractResponse> getUserContracts(Long user_id) {
		
		Iterable<Contract> contracts = contractRepository.findByUserId(user_id);
		//contracts.
        List<ContractResponse> response = StreamSupport.stream(contracts.spliterator(), true)
                .map(u -> new ContractResponse(u))
                .collect(Collectors.toList());
		
		return response;
	}

	public ContractResponse getContract(Long id) {
		
		Optional<Contract> contract = contractRepository.findById(id);
		
        /*List<ContractResponse> response = StreamSupport.stream(contracts.spliterator(), true)
                .map(u -> new ContractResponse(u))
                .collect(Collectors.toList());*/
		
		return new ContractResponse(contract.get());
	}
}
