package service;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.crypto.spec.SecretKeySpec;

import org.bouncycastle.util.encoders.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import crypto.AESKeyGenerator;
import exeption.UserAlreadyExistsException;
import model.Contract;
import model.Role;
import model.Users;
import repository.RoleRepository;
import repository.UserRepository;
import response.ContractResponse;
import security.Argon2PasswordEncoder;

@Service
public class UserService {
	@Autowired
	private UserRepository userRepository;

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private CurrentUserProvider currentUserProvider;

	@Autowired
	private Argon2PasswordEncoder argon2PasswordEncoder;

	public UserService(UserRepository userRepository, Argon2PasswordEncoder argon2PasswordEncoder) {
		this.userRepository = userRepository;
		this.argon2PasswordEncoder = argon2PasswordEncoder;
	}

	public Users create(Users user) throws NoSuchAlgorithmException, UserAlreadyExistsException {

		if (null != userRepository.findByEmail(user.getEmail())) {
			throw new UserAlreadyExistsException();
		}
		byte[] passwordToHash = Base64.decode(user.getPassword());
		byte[] aesKey = Arrays.copyOfRange(passwordToHash, 0,  31);//obtenemos la mitad del password para generar clave para aes
		SecretKeySpec secretKeySpec = AESKeyGenerator.generate(aesKey);
		String phoneNumber = "";
		String passwordHashed = new String(Arrays.copyOfRange(passwordToHash, 31, passwordToHash.length));
		try {
			phoneNumber = AESKeyGenerator.encryptAes(user.getPhone(), secretKeySpec);
			user.setPhone(phoneNumber);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String hashPassword = argon2PasswordEncoder.encode(passwordHashed);

		user.setUsername(user.getUsername());
		user.setPhone(user.getPhone());
		user.setPassword(hashPassword);
		user.setEmail(user.getEmail());

		user.setRoles(Arrays.asList(roleRepository.findByName("ROLE_USER")));

		user.setPassword(hashPassword);
		user.setEnabled(true);

		return userRepository.save(user);

	}

	public List<ContractResponse> getUserContract() {
		Users currentUser = currentUserProvider.getCurrentUser();
		Users user = userRepository.findByEmail(currentUser.getEmail());
		Iterable<Contract> contracts = user.getCotrats();
		// System.out.println(user.getCotrats());

		/*
		 * for(Contract contract: contracts) { contract.getServices() }
		 */
		List<ContractResponse> response = StreamSupport.stream(contracts.spliterator(), true)
				.map(u -> new ContractResponse(u)).collect(Collectors.toList());

		return response;
	}
}
