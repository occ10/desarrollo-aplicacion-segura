-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 24-05-2019 a las 00:44:33
-- Versión del servidor: 10.1.38-MariaDB
-- Versión de PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `das_example`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contract`
--

CREATE TABLE `contract` (
  `id` bigint(20) NOT NULL,
  `date` date DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `contract`
--

INSERT INTO `contract` (`id`, `date`, `user_id`) VALUES
(1, '2019-05-13', 20),
(2, '2019-05-14', 20);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contrat_services`
--

CREATE TABLE `contrat_services` (
  `contratid` bigint(20) NOT NULL,
  `servicioid` bigint(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `contrat_services`
--

INSERT INTO `contrat_services` (`contratid`, `servicioid`) VALUES
(1, 8),
(1, 29);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hibernate_sequence`
--

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(34),
(34),
(34),
(34),
(34),
(34),
(34);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `privilege`
--

CREATE TABLE `privilege` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `privilege`
--

INSERT INTO `privilege` (`id`, `name`) VALUES
(16, 'READ_PRIVILEGE'),
(17, 'WRITE_PRIVILEGE');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `role`
--

CREATE TABLE `role` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `role`
--

INSERT INTO `role` (`id`, `name`) VALUES
(18, 'ROLE_ADMIN'),
(19, 'ROLE_USER');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles_privileges`
--

CREATE TABLE `roles_privileges` (
  `role_id` bigint(20) NOT NULL,
  `privilege_id` bigint(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `roles_privileges`
--

INSERT INTO `roles_privileges` (`role_id`, `privilege_id`) VALUES
(18, 16),
(18, 17),
(19, 16);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `services`
--

CREATE TABLE `services` (
  `id` bigint(20) NOT NULL,
  `brute_price` double DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `type_service` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `services`
--

INSERT INTO `services` (`id`, `brute_price`, `name`, `type_service`) VALUES
(5, 39.67, 'TARIFA PLUS 12/4', 1),
(6, 19.84, 'TARIFA ECO 6/2', 1),
(7, 29.75, 'TARIFA BASICA 8/3', 1),
(8, 299, 'Conexión a Internet garantizada 40 UL + ', 1),
(9, 0, 'CREDITOS', 1),
(10, 99, 'TARIFA SIMéTRICA 10/10', 1),
(11, 99.17, 'Tarifa Profesional 10/10', 1),
(12, 100, 'CONEXIóN WIFI PúBLICA', 1),
(13, 9.09, 'FIJO', 2),
(14, 20, 'Fijo Tarifa Internacional (TS)', 2),
(15, 12, 'Fijo 1000 (TS)', 2),
(21, 39.67, 'TARIFA PLUS 12/4', 1),
(22, 19.84, 'TARIFA ECO 6/2', 1),
(23, 29.75, 'TARIFA BASICA 8/3', 1),
(24, 299, 'Conexión a Internet garantizada 40 UL + ', 1),
(25, 0, 'CREDITOS', 1),
(26, 99, 'TARIFA SIMéTRICA 10/10', 1),
(27, 99.17, 'Tarifa Profesional 10/10', 1),
(28, 100, 'CONEXIóN WIFI PúBLICA', 1),
(29, 9.09, 'FIJO', 2),
(30, 20, 'Fijo Tarifa Internacional (TS)', 2),
(31, 12, 'Fijo 1000 (TS)', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `type_services`
--

CREATE TABLE `type_services` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `type_services`
--

INSERT INTO `type_services` (`id`, `name`) VALUES
(1, 'Internet'),
(2, 'Telefonía Fija'),
(3, 'Telefonía Movil'),
(4, 'Otros');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` bigint(20) NOT NULL,
  `email` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `enabled` bit(1) DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `email`, `enabled`, `password`, `username`) VALUES
(20, 'test@test.com', b'1', '$argon2i$v=19$m=65536,t=2,p=1$7vGtn2FYjGDgSTVLTg1Smg$W1/0QRgGY/f9Ml96x0+au5L1SZAOuWAhxxGgKu1rO5I', 'Test'),
(33, 'infomanu@gmail.com', b'1', '$argon2i$v=19$m=65536,t=2,p=1$megFxyMp4/iCa09BOu+jsw$o2J+3RvXLYPASPt/znwffNBv6pqVmbAMenGNv4N0x+w', 'das');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users_roles`
--

CREATE TABLE `users_roles` (
  `user_id` bigint(20) NOT NULL,
  `role_id` bigint(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `users_roles`
--

INSERT INTO `users_roles` (`user_id`, `role_id`) VALUES
(20, 18),
(33, 19);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `contract`
--
ALTER TABLE `contract`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKi6rphdb5rpnqnrp5twyk83jao` (`user_id`);

--
-- Indices de la tabla `contrat_services`
--
ALTER TABLE `contrat_services`
  ADD PRIMARY KEY (`contratid`,`servicioid`),
  ADD KEY `FKau0feeyo255fpworpqy5c8ov7` (`servicioid`);

--
-- Indices de la tabla `privilege`
--
ALTER TABLE `privilege`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `roles_privileges`
--
ALTER TABLE `roles_privileges`
  ADD KEY `FK5yjwxw2gvfyu76j3rgqwo685u` (`privilege_id`),
  ADD KEY `FK9h2vewsqh8luhfq71xokh4who` (`role_id`);

--
-- Indices de la tabla `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKtaw1m9ihl3pghanle0yi0o6xu` (`type_service`);

--
-- Indices de la tabla `type_services`
--
ALTER TABLE `type_services`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_6dotkott2kjsp8vw4d0m25fb7` (`email`);

--
-- Indices de la tabla `users_roles`
--
ALTER TABLE `users_roles`
  ADD KEY `FKt4v0rrweyk393bdgt107vdx0x` (`role_id`),
  ADD KEY `FK2o0jvgh89lemvvo17cbqvdxaa` (`user_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
